# Minerva plugin creation with starter-kit

Minerva plugin is a javascript file which manages its dedicated space in the Minerva page (right hand side panel). Since the plugin is a single Javascript file, all its content needs to be added/updated with JavaScript (no HTML pages). The same holds for CSS which either needs to be added via JavaSscript, or bundled using NPM (that is what starter-kit is doing), Gulp, Webpack or other similar technologies.

## General comments

* Minerva uses jQuery so plugins can use it as well since it is loaded in the global scope
* Minerva uses Bootstrap so Bootstrap styles are available to plugins as well
* Many of the functions which are used to interact with Minerva are asynchronous and thus return a [promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) and not the actual data. This holds mainly for functions for data retrieval such as `getAllBioentities` (see below).

## Building the starter-kit

The starter-kit example uses NPM (node package manager) and Browserify (and few other packages such as browserify-css or uglify) to build the plugin distriubtion file which you can publish at a URL address which can then be referenced from Minerva. To build the starter-kit you need to do the following steps:

* Download the starter-kit
* Download and install [NPM](https://nodejs.org/en/download/) if it is not yet installed on your system (try running `npm` from command line to find out)
* run `npm install` (this will create the *node_modules* directory with the required NPM modules)
* run `npm run build` to build the plugin which will be available in the *dist* directory
* run `npm run build-debug` if you want to obtained non-minified version of the compiled plugin, which can be used for 
debugging the plugin in, e.g., Google Chrome DevTools
* publish the resulting *plugin.js* file somewhere where it can be accessed by Minerva (please beware that if the instance is running on HTTPS, the plugin must be also accessible through HTTPS)

For the development process you might want to have your local instance of Minerva running, so that you do not have to publish the plugin file every time you do a change. However, if you do not have a local Minerva instance, you can use command line to commit and push your plugin to, e.g., GitHub and provide Minerva address of the raw file. So for example if your GitHub repository with the plugin is [https://github.com/davidhoksza/minerva-plugins-starter-kit/](https://github.com/davidhoksza/minerva-plugins-starter-kit/) you can run

```
npm run build && git commit -m "distribution commit" dist/plugin.js && git push
```

which will build your plugin and commit and push it to the Git repository (GitHub in our case) and your plugin will be available at [https://raw.githubusercontent.com/davidhoksza/minerva-plugins-starter-kit/master/dist/plugin.js](https://raw.githubusercontent.com/davidhoksza/minerva-plugins-starter-kit/master/dist/plugin.js)

## Plugin structure

The starter-kit contains CSS with styles sheets and JavaScript code. The kit actually uses SCSS ([Sass](https://sass-lang.com/) extension of CSS) which is then compiled into CSS during the build process. The JavaScript code consists of a single `index.js` file which:

* Registers required functions with Minerva
* Creates plugin's HTML structure
* Interacts with Minerva to do what needs to be done

#### Registering with Minerva

When the plugin is loaded into Minerva `minervaDefine` function is called, so this needs to be present in every plugin 
script. This function needs to return an object which maps keys `register`, `unregister`, `getName` 
and `getVersion` to the corresponding functions. Only the `register` function is passed an 
argument being the Minerva object which can then later be used to interact with the map. Additional 
arguments which can be passed are plugin's `minWidth` `defualtWidth`.

#### Creating plugin's HTML structure

The Minerva object passed to the `register` function contains the `element` attribute being a jQuery object 
corresponding to the DOM element holding the plugin container (Minerva uses jQuery, so plugins do not need to include it). 
With the container element in hand, the plugin can add and modify its content freely. Of course, the plugin can also 
modify Minerva's DOM elements, however we strongly discourage from that.

#### Interacting with Minerva

###### Minerva proxy object

All the interaction with Minerva should happen through the minerva proxy object or ServerConnector (see next section) passed to the `register` function. To explore this object, starter-kit logs it into the console (`console.log('minerva object ', minervaProxy);`) so after the plugin is loaded, you can check out your browser's developers console and go through it. The structure of the object is following (not all attributes are mentioned):

* configuration: includes information about available types of elements, reactions, miriam types, configuration options, map types and so on
* project:  content-related data and functionaliry
  * data: functions to retrieve the data, mainly `getAllBioEntities` and `getBioEntityById`
    * beware that most of these functions are asynchronous so they actually return a promise not the actual objects
  * map: functions to interact with the visual aspect of the map, mainly `showBioEntity` (highlights a bioentity), `hideBioEntity`, `fitBounds` (zooms to provided coordinates) and `addListener` (enables listening to events such as selection of entities) - see examples of using these functions in the starter-kit and the Minerva's [JavaScript API documntation](https://git-r3lab.uni.lu/piotr.gawron/minerva#javascript-api-unstable-dev-api).

An example of interaction with Minerva (see the `index.html` for more examples):

```
minervaProxy.project.data.getAllBioEntities().then(function(bes){ 
    console.log(bes);
    let be = bes[0];
    //the following line is valid only if the first bioineity is of type ALIAS and not REACTION
    minervaProxy.project.data.getBioEntityById({id:be.getId(), modelId:bw.getModelId(), type:'ALIAS'}).then( function(be1) {
        console.log(be1);
    });
```

Some of the functions are also described in the [JavaScript API documntation](https://git-r3lab.uni.lu/piotr.gawron/minerva#javascript-api-unstable-dev-api).

###### ServerConnector object

Minerva also exposes variable called `ServerConnector` to the global scope (therefore you can explore it by typing `ServerConnector` in the browser developers console). It provides various functionality such as ability to retrieve list of models, overlays, projects, link to logo file, server address or add and modify comments, users.

###### Minerva's API

It can happen that there exists a (mainly data-related) functionality which is not available in the proxy object but is available through [Minerva's REST API](https://git-r3lab.uni.lu/piotr.gawron/minerva). In such case you can use Ajax to retrieve the data (the easiest way is probably to use jQuery's [getJSON](http://api.jquery.com/jquery.getjson/) function).

## Supporting different Minerva versions

In Minerva v14 the version of [Bootstrap](https://getbootstrap.com/) was changed from v3 to v4. 
This means that if the plugin needs to target every instance of Minerva, it needs to take this account. If a plugin
does not take this into account and uses only Bootstrap v3 CSS classes and constructs, it will not display correctly 
in Minerva version >= 14 and vice versa, if it will use Boostrap v4 CSS classes and constructs it might have issues
with Minerva version < 14.

The starter-kit
plugin is an example of a plugin which is minerva version agnostic. When registering the plugin a version of Minerva
is obtained as:

```javascript
    return minerva.ServerConnector.getConfiguration().then( function(conf) {
        minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));        
        initPlugin();
    });
```

the `minervaVersion` variable can then be used to do conditional rendering of the HTML code. Examples of this can be seen, e.g., 
in the disease association plugin, where different HTML code is generated based on the ``if (minervaVersion < 14)`` condition.

However, in cases when using Bootstrap CSS which are mutual exclusive between v3 and v4, using of the 
`minervaVersion` is not necessary. For example the *panel* concept which existed
in Bootstrap v3 has been replaced by the *card* concept (see the 
[Bootstrap migration guide](https://getbootstrap.com/docs/4.0/migration/) to see all the changes) 
and the ``panel-*`` CSS classes have been replaced by `card-*` 
classes. You can see in the [starter-kit code](https://git-r3lab.uni.lu/minerva/plugins/starter-kit/blob/master/src/js/index.js#L106) 
that it uses both concepts alongside each other to support both versions of Bootstrap.
This means the layout will look similarly in any Minerva version. 
In cases, when the classes are not exclusive, i.e. one CSS
class has different meaning in Bootstrap v3 and v4 or the structure of the HTML needs to be different, 
one needs to use the minerva version information.
  