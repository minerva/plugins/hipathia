require('../css/styles.css');
var JSZip = require("jszip");
var FileSaver = require('file-saver');

const pluginName = 'hipathia';
const pluginVersion = '0.0.1';

const globals = {
  selected: [],
  allBioEntities: [],
  pickedRandomly: undefined
};

let $ = window.$;
if ($ === undefined && minerva.$ !== undefined) {
  $ = minerva.$;
}

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let minervaVersion;

let complexChild = [];


const register = function (_minerva) {

  console.log('registering ' + pluginName + ' plugin');

  $(".tab-content").css('position', 'relative');

  minervaProxy = _minerva;
  pluginContainer = $(minervaProxy.element);
  pluginContainerId = pluginContainer.attr('id');
  if (!pluginContainerId) {
    //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
    pluginContainerId = pluginContainer.parent().attr('id');
  }


  console.log('minerva object ', minervaProxy);
  console.log('project id: ', minervaProxy.project.data.getProjectId());
  console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

  return minerva.ServerConnector.getConfiguration().then(function (conf) {
    minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));
    console.log('minerva version: ', minervaVersion);
    initPlugin();
  });
};

const unregister = function () {
  console.log('unregistering ' + pluginName + ' plugin');

};

const getName = function () {
  return pluginName;
};

const getVersion = function () {
  return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function () {
  return {
    register: register,
    unregister: unregister,
    getName: getName,
    getVersion: getVersion,
    minWidth: 400,
    defaultWidth: 500
  }
});

function initPlugin() {
  initMainPageStructure();
}


// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function initMainPageStructure() {

  const container = $('<div class="' + pluginName + '-container"></div>').appendTo(pluginContainer);
  container.append('<button type="button" class="btn-pick-random btn btn-primary btn-default btn-block">Generate hipathia files</button>');

  container.find('.btn-pick-random').on('click', () => generateHipathia());
}

function generateHipathia() {
  return minervaProxy.project.data.getAllBioEntities().then(function (bioEntities) {
    let aliases = [];
    let reactions = [];
    for (let i = 0; i < bioEntities.length; i++) {
      if (bioEntities[i].constructor.name === 'Alias') {
        aliases.push(bioEntities[i]);
      } else {
        reactions.push(bioEntities[i]);
      }
    }
    complexChild = [];

    for (let i = 0; i < aliases.length; i++) {
      let alias = aliases[i];
      if (alias.getComplexId() !== undefined) {
        if (complexChild[alias.getComplexId()] === undefined) {
          complexChild[alias.getComplexId()] = [];
        }
        complexChild[alias.getComplexId()].push(alias);
      }
    }

    let aliasStrings = ["ID\tlabel\tX\tY\tcolor\tshape\ttype\tlabel.cex\tlabel.color\twidth\theight\tgenesList\n"];
    for (let i = 0; i < aliases.length; i++) {
      let alias = aliases[i];
      if (alias.getComplexId() === undefined) {
        aliasStrings.push(createAliasString(aliases[i], complexChild[aliases[i].getId()]));
      }
    }

    let reactionStrings = [];
    for (let i = 0; i < reactions.length; i++) {
      reactionStrings.push(createReactionString(reactions[i]));
    }

    var zip = new JSZip();

    zip.file("Hello.sif", reactionStrings.join(""));
    zip.file("Hello.att", aliasStrings.join(""));

    return zip.generateAsync({type: "blob"})
      .then(function (content) {
        // see FileSaver.js
        FileSaver.saveAs(content, "Hello.zip");
      });
  });
}

function createReactionString(reaction) {
  let result = "";
  for (let i = 0; i < reaction.getProducts().length; i++) {
    let product = reaction.getProducts()[i].getAlias();
    for (let j = 0; j < reaction.getReactants().length; j++) {
      let reactant = reaction.getReactants()[j].getAlias();
      result += getAliasId(reactant.getId()) + "\t" + getReactionType(reaction.getType()) + "\t" + getAliasId(product.getId()) + "\n";
    }
    // there is no type defined for modifier so we ignore it for now
    // for (let j = 0; j < reaction.getModifiers().length; j++) {
    //     let reactant = reaction.getModifiers()[j].getAlias();
    //     result += reactant.getElementId() + "\t" + getReactionType(reaction.getType()) + "\t" + product.getElementId() + "\n";
    // }
  }
  return result;
}

function getAliasType(alias) {
  let type = "other";
  if (alias.getType() === "Gene" || alias.getType() === "Protein") {
    type = "gene";
  }
  return type;
}

function getChildrenId(children) {
  let ids = [];
  for (let i = 0; i < children.length; i++) {
    ids.push(children[i].getId() + "");
  }
  return ids.join(" ");
}

function getAliasId(id) {
  let result = id;
  if (complexChild[id] !== undefined) {
    let children = complexChild[id];
    let ids = [];
    for (let i = 0; i < children.length; i++) {
      ids.push(children[i].getId() + "");
    }
    result = ids.join(" ");
  }
  return "N-" + minervaProxy.project.data.getProjectId() + "-" + result;
}

function getAliasEntrezStringList(alias) {
  let genes = [];
  for (let j = 0; j < alias.getReferences().length; j++) {
    if (alias.getReferences()[j].getType() === "ENTREZ") {
      genes.push(alias.getReferences()[j].getResource());
    }
  }
  let genesString = "N/A";
  if (genes.length > 0)
    genesString = genes.join(",");
  return genesString;
}

function createAliasString(alias, children) {
  let result = "";
  let type = "other";
  let genesString = "N/A";
  let id = getAliasId(alias.getId());
  if (children === undefined) {
    type = getAliasType(alias);
    genesString = getAliasEntrezStringList(alias);
  } else {
    let types = [];
    let genes = [];
    for (let i = 0; i < children.length; i++) {
      types.push(getAliasType(children[i]));
      genes.push(getAliasEntrezStringList(children[i]));
    }
    type = types.join(",");
    genesString = genes.join(",/,");
  }
  result = id + "\t" +
    alias.getName() + "\t" +
    alias.getX() + "\t" +
    alias.getY() + "\t" +
    "white\t" +
    "rectangle\t" +
    type + "\t" +
    "0.5\t" +
    "black\t" +
    alias.getWidth() + "\t" +
    alias.getHeight() + "\t" +
    genesString + "\n";

  return result;
}

var reactionType = [];
reactionType["Heterodimer association"] = "activation";
reactionType["Catalysis"] = "activation";
reactionType["Dissociation"] = "activation";
reactionType["Inhibition"] = "inhibition";
reactionType["Known transition omitted"] = "activation";
reactionType["Modulation"] = "activation";
reactionType["Negative influence"] = "inhibition";
reactionType["Physical stimulation"] = "activation";
reactionType["Positive influence"] = "activation";
reactionType["Reduced modulation"] = "activation";
reactionType["Reduced physical stimulation"] = "activation";
reactionType["Reduced trigger"] = "activation";
reactionType["State transition"] = "activation";
reactionType["Transcription"] = "activation";
reactionType["Translation"] = "activation";
reactionType["Transport"] = "activation";
reactionType["Trigger"] = "activation";
reactionType["Truncation"] = "activation";
reactionType["Unknown catalysis"] = "activation";
reactionType["Unknown inhibition"] = "inhibition";
reactionType["Unknown negative influence"] = "inhibition";
reactionType["Unknown positive influence"] = "activation";
reactionType["Unknown reduced modulation"] = "activation";
reactionType["Unknown reduced physical stimulation"] = "activation";
reactionType["Unknown reduced trigger"] = "activation";
reactionType["Unknown transition"] = "activation";

var modifierType = [];

modifierType["Catalysis"] = "activation";
modifierType["Inhibition"] = "inhibition";
modifierType["Modulation"] = "activation";
modifierType["PhysicalStimulation"] = "activation";
modifierType["Trigger"] = "activation";
modifierType["UnknownCatalysis"] = "activation";
modifierType["UnknownInhibition"] = "inhibition";

function getReactionType(type) {
  if (reactionType[type] === undefined)
    return "N/A";
  else
    return reactionType[type];
}

function getModifierType(type) {
  if (modifierType[type] === undefined)
    return "N/A";
  else
    return modifierType[type];
}
